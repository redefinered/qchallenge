<?php

namespace App\Http\Controllers;

use App\Section;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SectionController extends Controller
{
    protected $request;
    protected $section;

    /**
     *
     * @param Request $request
     * @param Product $section
     */
    public function __construct(Request $request, Section $section) {
        $this->request = $request;
        $this->section = $section;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = $this->section->all();
        return response()->json(['data' => $sections,
            'status' => Response::HTTP_OK]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dump($request->all());
        $section = new Section();
        $section->name = $request->input('name');
        $section->order = $request->input('order');
        $addSectionSuccess = $section->save();
        echo $addSectionSuccess ? 'Section add success!' : 'Error';

        if ( $addSectionSuccess ) {
            $question = new Question();
            $question->section_id = $question_id;
            $question->question_text = $question_text;
            $question->input_type = $input_type;
            $question->order = $order;
            $response = $question->save();
            echo $response ? 'Question add success!' : 'Error';

            $answer = new Answer();
            $answer->question_id = $question_id;
            $answer->value = $value;
            $response = $answer->save();
            echo $response ? 'Answer add success!' : 'Error';
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        //
    }
}
